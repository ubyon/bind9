#!/bin/bash

set -e
set -x

git submodule update --init --force --remote

# Build the udns service.
tools/build.sh -j4

# Do the packaging work using Makefile.ci.
make -f Makefile.ci
