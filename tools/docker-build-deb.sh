#!/bin/bash

set -e
set -x

# Run the build with the same uid as the outside user so that
# the build output has the same permission.
extuid=$(stat -c %u /buildroot)
extgid=$(stat -c %g /buildroot)
if [ $(id -u) != "$extuid" ]; then
  groupadd build --gid $extgid || true
  useradd build --groups sudo \
    --home-dir /buildroot \
    --uid $extuid \
    --gid $extgid \
    --no-create-home || true
  su build -- "$0" "$@"
  exit 0
fi

cd bind9-src

if [ X$1 = "Xbash" ]; then
  /bin/bash
else
  autoreconf -i
  ./configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info \
    --sysconfdir=/etc/bind --localstatedir=/var --enable-largefile \
    --enable-shared --with-openssl=/usr  --with-gnu-ld \
    CFLAGS='-fno-strict-aliasing -g3 -ggdb -O0' --with-libxml2 --enable-dnstap \
    --enable-linux-caps --without-python
  make "$@"
fi
