#!/bin/bash
#
# Script to do build in docker.
#

set -e
set -x

cd $(dirname $0)/..

BUILD_ROOT=./
BUILD_CONTAINER=quay.io/ubyon/ubuntu20-builder
BUILD_COMMAND=/buildroot/tools/docker-build-deb.sh

echo "Launching ${BUILD_CONTAINER} ${DOCKER_BUILD_COMMAND}"
docker run \
  --rm \
  --tty \
  --interactive \
  --volume $(readlink -f "${BUILD_ROOT}"):/buildroot:z \
  --env BUILD_ROOT=/buildroot \
  --env CMAKE_NO_VERBOSE=1 \
  --env VERBOSE=$VERBOSE \
  --workdir /buildroot \
  ${BUILD_CONTAINER} \
  ${BUILD_COMMAND} "$@"
